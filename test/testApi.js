var mocha=require('mocha');
var chai=require('chai');
var chaiHttp=require('chai-http');
var server=('../server_v2')
var should =chai.should();

chai.use(chaiHttp) //Configurar chai con mòdulo HTTP

describe('Pruebas Colombia',() => {

  it('BBVA Funciona ', (done) => {
    chai.request('http://www.bbva.com')
      .get('/')
      .end((err,res)=>{
        //console.log('res '+res+" err "+ err);
        res.should.have.status(200);
        //res.header['x-amz-cf-id']
          done();
      });
  });

  it('Mi API funciona ', (done) => {
    chai.request('http://localhost:3005')
      .get('/colapi/v3/users')
      .end((err,res)=>{
        res.should.have.status(200);
        res.body.should.be.a('array');
        res.body.length.should.be.gte(1);
        res.body[0].should.have.property('name')
        //console.log(res.body);
          done();
      });
  });


/*  it('Validar crear Elemento', (done) => {
    chai.request('http://localhost:3005')
      .post('/colapi/v3/login')
      .send('{"email":"mich@hot.com","password":"123"}')
      .end((err,res,body)=>{
        res.should.have.status(200);
        /*res.body.should.be.a('array');
        res.body.length.should.be.gte(1);
        res.body[0].should.have.property('name')
          console.log(res.body);
          console.log(body);
          done();
      });
  });*/

});
