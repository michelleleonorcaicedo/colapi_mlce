var express = require('express');
var bodyParser = require('body-parser');//parsear el json del body
var app = express();
var port= process.env.PORT || 3005;//utilizar el puerto de varible de entorno o el por defecto 3000
var usersFile= require('./users.json');
//bibliotecas para consumir servicios get
var requestJson= require('request-json');
var URLbase="/colapi/v3/";
var baseMlabURL="https://api.mlab.com/api/1/databases/colapidb_mlce/collections/";
var apiKey="L2YdfHQgn9OZ8aaZ7szeDWt2HBgbdpVd";

app.listen(port, function () {
  console.log('Colapi escuchando por el puerto ! '+ port);
});

app.use(bodyParser.json());

app.get(URLbase+"accounts",
  function(req, res){
    console.log("GET /colapi/v3/ ");
    var httpClient=requestJson.createClient(baseMlabURL);
    httpClient.get('account?apiKey='+apiKey,
      function(err, respMlab, body){

        console.log('Respusta MLab: '+ respMlab);
        var respuesta=body;
        res.send(respuesta);
      });
});


app.get(URLbase+"users",
  function(req, res){
    console.log("GET /colapi/v3/ ");
    var httpClient=requestJson.createClient(baseMlabURL);
    var queryString='f={_id:0}&';
    httpClient.get('user?'+queryString+'apiKey='+apiKey,
      function(err, respMlab, body){

        console.log('Respusta MLab: '+ respMlab);
        var respuesta=body;
        res.send(respuesta);
      });
});

app.get(URLbase+"movements",
  function(req, res){
    console.log("GET /colapi/v3/ ");
    var httpClient=requestJson.createClient(baseMlabURL);
    var queryString='f={_id:0}&';
    httpClient.get('movement?'+queryString+'apiKey='+apiKey,
      function(err, respMlab, body){

        console.log('Respusta MLab: '+ respMlab);
        var respuesta=body;
        res.send(respuesta);
      });
});

app.get(URLbase+"users/:id",
  function(req, res){
    var id=req.params.id;
    var httpClient=requestJson.createClient(baseMlabURL);
    //var queryString="";
    httpClient.get("user?q={'id':"+id+"}&apiKey="+apiKey,
      function(err, respMlab, body){
        var respuesta=body;
        res.send(respuesta);
      });
});

app.get(URLbase+"movements/:iban",
  function(req, res){
    var iban=req.params.iban;
    var httpClient=requestJson.createClient(baseMlabURL);
    //var queryString="";
    httpClient.get("movement?q={'IBAN':'"+iban+"'}&apiKey="+apiKey,
      function(err, respMlab, body){
        var respuesta=body;
        console.log(respuesta);
        res.send(respuesta);
      });
});

app.get(URLbase+"users/:id/accounts/:iban",
  function(req, res){
    var id = req.params.id;
    var iban = req.params.iban;
    console.log("GET USER id "+id+" iban "+iban);
    var httpClient=requestJson.createClient(baseMlabURL);
    //var queryString="q={'user_id':'"+id+"','IBAN':"+iban+"'}&";
    var queryString="f={'_id':0}&q={'IBAN':'"+iban+"'}&";
    console.log(queryString);
    httpClient.get('account?'+queryString+'apiKey='+apiKey,
      function(err, respMlab, body){
        console.log(err);
        res.send(body);
      });
  });

app.get(URLbase+"users/:id/accounts",
  function(req, res){
    var id = req.params.id;
    var iban = req.params.iban;
    console.log("GET USER id "+id);
    var httpClient=requestJson.createClient(baseMlabURL);
    var queryString="q={'user_id':"+id+"}&";
    console.log(queryString);
    httpClient.get('account?'+queryString+'apiKey='+apiKey,
      function(err, respMlab, body){
        console.log(err);
        res.send(body);
      });
  });


//  PUT y POST con mLab:

  app.post(URLbase+'accounts',
   function(req, res) {
     var clienteMlab = requestJson.createClient(baseMlabURL + "account?apiKey=" + apiKey);
     var cuentaNueva=req.body;
     console.log('cuenta Nueva '+cuentaNueva);
     clienteMlab.post('', cuentaNueva,
       function(err, resM, body) {
         res.send(body);//enviar código
       });
 });

//PUT LOGGIN
  app.put(URLbase+'accounts/:id',
    function(req, res) {
     var id=req.params.id;
     var clienteMlab = requestJson.createClient(baseMlabURL + "account");
     //modificacion, toca poner el dolar para el mlab
     var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
     console.log('pruebas cambios'+cambio)
     clienteMlab.put('?q={"user_id": ' + id + '}&apiKey=' + apiKey,
       JSON.parse(cambio),
        function(err, resM, body) {
          console.log('err '+err+' resM '+ resM);
         res.send(body);
     });
 });

 //PUT USER
   app.put(URLbase+'users/:id',
     function(req, res) {
      var id=req.params.id;
      var clienteMlab = requestJson.createClient(baseMlabURL + "user");
      //modificacion, toca poner el dolar para el mlab
      var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
      console.log('pruebas cambios'+cambio)
      clienteMlab.put('?q={"id": ' + id + '}&apiKey=' + apiKey,
        JSON.parse(cambio),
         function(err, resM, body) {
           console.log('err '+err+' resM '+ body);
           if(body.n=='1'){
          res.send(201,req.body);
            }else{
              res.send(404,body);
            }

      });
  });

//POST LOGIN
 app.post(URLbase+'login',
   function(req, res) {
    var email=req.body.email;
    var password=req.body.password;
    var id;
    var loggin=false;
    var queryString='q={"email":"'+email+'","password":"'+password+'"}&';
    console.log(queryString);
    //GET CONSULTAR PASS Y EMAIL
    var httpClient=requestJson.createClient(baseMlabURL);
    httpClient.get('user?'+queryString+'apiKey='+apiKey,
      function(err, respMlab, body){
        if(body[0]!=undefined&&body[0].email==email&&body[0].password==password){
          console.log("pruebas body mich "+body[0].email);
            var cambio = '{"$set":{"loggin":true}}';
            var bodyUser=body;
            console.log('pruebas cambios '+cambio);

            //PUT agregar login
            httpClient.put('user?q={"email":"'+email+'"}&apiKey=' + apiKey,
              JSON.parse(cambio),
               function(err, resM, bodyP) {
                res.send({"msg":"Bienvenido", "id":bodyUser[0].id});
                console.log(bodyP);
            });
        }else{
          res.send({"msg":"Usuario o contraseña incorrecta"});
        }
      });
});


app.post(URLbase+'logout',
  function(req, res) {
   var email=req.body.email;
   //GET CONSULTAR PASS Y EMAIL
   var httpClient=requestJson.createClient(baseMlabURL);
           //PUT agregar login
           var cambio = '{"$unset":{"loggin":""}}';
           httpClient.put('user?q={"email":"' + email + '"}&apiKey='+ apiKey,
             JSON.parse(cambio),
              function(err, resM, body) {
                if(body.n=='1'){
                  console.log(body.n);
                  res.send({"msg":"sesión cerrada"});
                }else{
                  res.send({"msg":"Invalido"});
                }
           });
});
