#Imagen inicial a partir de la cual creamos nuestra imagen
FROM node

#Creamos nuestro directorio del contenedor
WORKDIR /colapi_mlce

#Añadimos contenido del proyecto en directorio del contenedor
ADD . /colapi_mlce

#Puerto va a escuchar, debe ser igual al definido en colapi
EXPOSE 3000

#Comandos para lanzar nuestra API REST colapi
CMD ["npm","start"]
#CMD ['node', 'server_2.js']
